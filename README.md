## Project setup
```
cd ticketLemon

npm install
```

### Compiles and run for development
```
npm start
```

### Support requirement feature
```
- fetch data from api
- gender ratio on pie chart
- age ratio on bar chart 
- datatable with sortable (Sorting by press on key what you wish such as Person ID, Name, Gender, Age)
```

### Not Support requirement feature
```
- search on datatable
```

### Warning
```
This project was run on iOS simulator only (iPhone11 pro Max-iOS 13.2.2)
```