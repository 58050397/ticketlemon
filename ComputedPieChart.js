import React, {Component} from 'react'
import { StyleSheet, Dimensions} from 'react-native'
import { PieChart } from 'react-native-chart-kit'

class ComputedPieChart extends Component {

    //  generate gender population dataset object
    getGenderPopulation() {
        let genderPopulation = {
            male: 0,
            female: 0
        }
        this.props.customersList.forEach((customer) => {
            if (customer.gender == 'male') {
                genderPopulation.male += 1
            } else {
                genderPopulation.female +=1
            }
        })
        return genderPopulation
    }

    //  render computed pie chart for customer list
    render () {
        const genderPopulation = this.getGenderPopulation()
        const genderPopulationDataset = [
            {
                name: 'male',
                population: genderPopulation.male,
                color: 'rgba(255, 255, 255, 0.8)',
                legendFontColor: "white",
                legendFontSize: 15
            },
            {
                name: 'female',
                population: genderPopulation.female,
                color: 'rgba(255, 255, 255, 0.4)',
                legendFontColor: "white",
                legendFontSize: 15
            }
        ]
        const genderPopulationChartConfig = {
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`
        }

        return (
            <PieChart
                data={genderPopulationDataset}
                width={Dimensions.get("window").width-20}
                height={220}
                chartConfig={genderPopulationChartConfig}
                backgroundColor="rgba(12, 5, 163, 0.5)"
                accessor="population"
                style={styles.pieChart}
            />
        )
    }
}
export default ComputedPieChart

const styles = StyleSheet.create({
    pieChart: {
        marginBottom: 20,
        borderRadius: 16
    }
});


