import React, {Component} from 'react'
import { StyleSheet, Dimensions} from 'react-native'
import { BarChart } from 'react-native-chart-kit'

class ComputedBarChart extends Component {

    //  generate age population dataset object
    getAgePopulation() {
        let agePopulation = {
            child: 0,   // range age 0 - 20
            youngAdult: 0,   // range age 21 - 40 
            adult: 0,   // range age 41 - 60 
            old: 0,   // range age 61 - 80 
            grandOld: 0,   // range age 81 - 100
        }
        this.props.customersList.forEach((customer) => {
            if (customer.age > 80) {
                agePopulation.grandOld += 1
            } else if (customer.age > 60) {
                agePopulation.old += 1
            } else if (customer.age > 40) {
                agePopulation.adult += 1
            } else if (customer.age > 20) {
                agePopulation.youngAdult += 1
            } else {
                agePopulation.child += 1
            }
        })
        return agePopulation
    }

    //  render computed bar chart for customer list
    render () {
        const agePopulation = this.getAgePopulation()
        const agePopulationDataset = {
            labels: ['0-20', '21-40', '41-60', '61-80', '81-100'],
            datasets: [
                {
                    data: [agePopulation.child, agePopulation.youngAdult, agePopulation.adult, agePopulation.old, agePopulation.grandOld]
                }
            ]
        }
        const agePopulationChartConfig = {
            backgroundGradientFrom: '#0c05a3',
            backgroundGradientTo: '#0c05a3',
            backgroundGradientFromOpacity: 0.5,
            backgroundGradientToOpacity: 0.7,
            decimalPlaces: 0,
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        }
        return (
            <BarChart 
                data={agePopulationDataset} 
                width={Dimensions.get("window").width-20} 
                height={220}
                chartConfig={agePopulationChartConfig}
                fromZero
                style={styles.barChart}
            />
        )
    }
}
export default ComputedBarChart

const styles = StyleSheet.create({
    barChart: {
        marginBottom: 20,
        borderRadius: 16
    }
})


