import React, {Component} from 'react'
import {StyleSheet, Text, View, ScrollView} from 'react-native'
import ComputedPieChart from './ComputedPieChart'
import ComputedBarChart from './ComputedBarChart'
import DataTableWithSortable from './DataTableWithSortable'

export default class App extends Component {

    // initial state for App 
    constructor(props){
        super(props)
        this.state ={ 
            loading: true,
            customersList: []
        }
    }

    // fetch data from mock api
    componentWillMount(){
        fetch('http://5dd287ef6625890014a6dbc7.mockapi.io/customers')
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                loading: false,
                customersList: responseJson
            })
        })
        .catch((error) =>{
            console.error(error);
        });
    }
    
    //  render app 
    render () {
        if(this.state.loading) {
            return <Text>loading...</Text>
        }
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.title}>Customers gender ratio</Text>
                    <ComputedPieChart customersList={this.state.customersList}/>
                    <Text style={styles.title}>Customers age ratio</Text>
                    <ComputedBarChart customersList={this.state.customersList}/>
                    <Text style={styles.title}>Customers table</Text>
                    <DataTableWithSortable customersList={this.state.customersList}/>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 75,
  },
  title: {
    fontSize: 20,
    marginBottom: 20
  }
})
