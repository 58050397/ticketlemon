import React, {Component} from 'react'
import { DataTable } from 'react-native-paper'

class DataTableWithSortable extends Component {

    //  initial state for DataTableWithSortable
    constructor(props){
        super(props)
        this.state ={ 
            customersList: this.props.customersList,
            sortingBy: {
                key: null,
                order: null
            }
        }
    }
    
    //  compare function for number and string
    compareValues(key, order='asc') {
        return function(a, b) {
          if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
            // property doesn't exist on either object
            return 0;
          }
      
          const varA = (typeof a[key] === 'string') ?
            a[key].toUpperCase() : a[key];
          const varB = (typeof b[key] === 'string') ?
            b[key].toUpperCase() : b[key];
      
          let comparison = 0;
          if (varA > varB) {
            comparison = 1;
          } else if (varA < varB) {
            comparison = -1;
          }
          return (
            (order == 'desc') ? (comparison * -1) : comparison
          );
        };
    }
    
    //  sorting function for sort customer list that depend on key
    sortTableBy(key) {
        if(this.state.sortingBy.key != key) {
            this.state.customersList.sort(this.compareValues(key))
            this.setState(
                {
                    customersList:this.state.customersList,
                    sortingBy:{
                        key: key,
                        order: 'ascending'
                    }
                }
            )
        } else {
            if(this.state.sortingBy.order == 'ascending') {
                this.state.customersList.sort(this.compareValues(key,'desc'))
                this.setState(
                    {
                        customersList:this.state.customersList,
                        sortingBy:{
                            key: key,
                            order: 'descending'
                        }
                    }
                )
            } else {
                this.state.customersList.sort(this.compareValues(key))
                this.setState(
                    {
                        customersList:this.state.customersList,
                        sortingBy:{
                            key: key,
                            order: 'ascending'
                        }
                    }
                )
            }
        }
    }

    //  generate DataTable.Row and DataTable.Cell from customers list
    getDataTableRowForCustomers () {
        let dataTableRowForCustomers = []
        for(let index = 0; index < this.state.customersList.length; index++) {
            dataTableRowForCustomers.push (
                <DataTable.Row key={this.state.customersList[index].personId}>
                    <DataTable.Cell>
                        {this.state.customersList[index].personId}
                    </DataTable.Cell>
                    <DataTable.Cell>
                        {this.state.customersList[index].name}
                    </DataTable.Cell>
                    <DataTable.Cell>
                        {this.state.customersList[index].gender}
                    </DataTable.Cell>
                    <DataTable.Cell>
                        {this.state.customersList[index].age}
                    </DataTable.Cell>
                </DataTable.Row>
            )
        }
        return dataTableRowForCustomers
    }

    //  render DataTable that can sortable
    render () {
        return (
            <DataTable>
                <DataTable.Header>
                    <DataTable.Title onPress= {()=>{this.sortTableBy('personId')}}>Person ID</DataTable.Title>
                    <DataTable.Title onPress= {()=>{this.sortTableBy('name')}}>Name</DataTable.Title>
                    <DataTable.Title onPress= {()=>{this.sortTableBy('gender')}}>Gender</DataTable.Title>
                    <DataTable.Title onPress= {()=>{this.sortTableBy('age')}} numeric>Age</DataTable.Title>
                </DataTable.Header>
                {this.getDataTableRowForCustomers()}
            </DataTable>
        )
    }
}
export default DataTableWithSortable